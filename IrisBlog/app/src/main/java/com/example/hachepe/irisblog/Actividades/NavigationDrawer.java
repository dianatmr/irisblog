package com.example.hachepe.irisblog.Actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.hachepe.irisblog.Fragments.Biblioteca;
import com.example.hachepe.irisblog.Models.Entrada;
import com.example.hachepe.irisblog.Otras_Clases.Acercade;
import com.example.hachepe.irisblog.Otras_Clases.AdaptadorItem;
import com.example.hachepe.irisblog.Otras_Clases.Pager;
import com.example.hachepe.irisblog.Otras_Clases.Salir;
import com.example.hachepe.irisblog.R;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // VARIABLES CONSTANTES
    private final String BIBLIOTECA = "Biblioteca";
    private final String EXPLORAR = "Explorar";
    
    // VARIABLES
    private int id;
    private TextView usuarioayuda;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        usuarioayuda=(TextView) findViewById(R.id.textView3);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        cargarToolbar();
        cargarBotonFlotante();
        cargarDrawerLayout();
        cargarNavigationView();
        inicializarTabLayout();
        if (extras != null) {//ver si contiene datos
            usuarioayuda.setText(((String)extras.get("Usuario")));
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Método que lanza cada actividad según el item seleccionado del menú lateral
     *
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            // Abre actividad con el perfil del usuario
            Intent i = new Intent(getApplicationContext(), Perfil.class);
            startActivity(i);
        } else if (id == R.id.nav_seguidores) {
            // Abre la actividad con...los seguidores por ejemplo
        } else if (id == R.id.nav_ajustes) {
            // Abre la actividad con los ajustes de la aplicación
            Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_acercaDe) {
            // Abre la actividad sobre Iris Blog
            Intent i = new Intent(getApplicationContext(), Acercade.class);
            startActivity(i);
        } else if (id == R.id.nav_salir) {
            // Cierra la sesión del usuario
            Intent i = new Intent(this, Salir.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Método que carga la Toolbar y la personaliza
     */
    private void cargarToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Iris Blog");
        setSupportActionBar(toolbar);
    }

    /**
     * Método que carga el botón flotante y el método onClick
     */
    private void cargarBotonFlotante() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_action_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Crear_entrada.class);
                i.putExtra("Usuario",usuarioayuda.getText().toString());
                startActivity(i);
            }
        });
    }

    /**
     * Método que carga el DrawerLayout
     */
    private void cargarDrawerLayout() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    /**
     * Método que carga el Navigation View
     */
    private void cargarNavigationView() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * Mñetodo que inicializa el TabLayout
     */
    private void inicializarTabLayout() {
        // Inicializando tabLayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        // Añadiendo las tablas que usan el método addTab()
        tabLayout.addTab(tabLayout.newTab().setText(BIBLIOTECA));
        tabLayout.addTab(tabLayout.newTab().setText(EXPLORAR));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        // Inicializando viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);
        // Creando nuestro pager adapter
        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());
        // Añadiendo el adapter al pager
        viewPager.setAdapter(adapter);
        // Adding setupWithViewPager
        tabLayout.setupWithViewPager(viewPager);
    }

}