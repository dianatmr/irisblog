package com.example.hachepe.irisblog.Actividades;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hachepe.irisblog.R;

public class Perfil extends AppCompatActivity {

    // VARIBALES CONSTANTES
    private static final byte PICK_IMAGE = 100;

    // VARIBALES
    private Uri imageUri;

    private Toolbar toolbar;
    private ImageView imageView;
    private EditText descripcion;
    private ImageButton edit, save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        cargarImageView();
        cargarDescripcion();
        cargarToolbar();
        cargarImageButtonEdit();
        cargarImageButtonSave();
    }


    /**
     * Método que carga la toolbar e implementa el método onClick de la flecha de retroceso
     */
    private void cargarToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbarPerfil);
        toolbar.setTitle("Nombre del usuario");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Regresa a la vista principal
                finish();
            }
        });
    }

    /**
     * Método que carga el imageview y lo hace circular
     */
    private void cargarImageView() {
        imageView = (ImageView) findViewById(R.id.imageView_Perfil);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        /*Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gradientgreen);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        roundedBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(roundedBitmapDrawable);*/
    }

    /**
     * Método que abre la galería del usuario
     */
    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    /**
     * Método que carga la imagen seleccionada en el imageview
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            imageView.setImageURI(imageUri);
        }
    }

    /**
     * Método que carga la descripción del usuario
     */
    private void cargarDescripcion() {
        descripcion = (EditText) findViewById(R.id.et_Descripcion);
        descripcion.setEnabled(false);
    }

    /**
     * Método que carga el imagebutton de editar la descripción e implementa el método onClick
     */
    private void cargarImageButtonEdit() {
        edit = (ImageButton) findViewById(R.id.imageButtonEdit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descripcion.setEnabled(true);
                save.setVisibility(View.VISIBLE);
                edit.setVisibility(View.INVISIBLE);
            }
        });

    }

    /**
     * Método que carga el imagebutton de guardar la descripción e implementa el método onClick
     */
    private void cargarImageButtonSave() {
        save = (ImageButton) findViewById(R.id.imageButtonSave);
        save.setVisibility(View.INVISIBLE);
        descripcion.setText(descripcion.getText().toString());
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descripcion.setEnabled(false);
                save.setVisibility(View.INVISIBLE);
                edit.setVisibility(View.VISIBLE);
                descripcion.setText(descripcion.getText().toString());
            }
        });
    }


}
