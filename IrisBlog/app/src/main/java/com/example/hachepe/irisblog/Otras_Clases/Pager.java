package com.example.hachepe.irisblog.Otras_Clases;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.hachepe.irisblog.Fragments.Biblioteca;
import com.example.hachepe.irisblog.Fragments.Explorar;
import com.example.hachepe.irisblog.R;

/**
 * Created by Michael on 24/11/2017.
 */

public class Pager extends FragmentStatePagerAdapter {
    // Integer para contar el número de tablas
    int tabCount;
    private Context mContext;

    //Constructor de la clase
    public Pager(FragmentManager fm, int _tabCount) {
        super(fm);
        this.tabCount = _tabCount;
    }

    // Método sobreescrito que devuelve el item seleccionado
    @Override
    public Fragment getItem(int position) {
        // Devuelve la tabla correspondiente
        switch (position) {
            case 0:
                Biblioteca biblioteca = new Biblioteca();

                return biblioteca;
            case 1:
                Explorar explorar = new Explorar();
                return explorar;
            default:
                return null;
        }
    }


    // Método sobreescrito getCount que devuelve el número de tablas
    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Biblioteca";
            case 1:
                return "Explorar";
        }
        return null;
    }
}
