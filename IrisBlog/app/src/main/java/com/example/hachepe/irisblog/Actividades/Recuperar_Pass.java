package com.example.hachepe.irisblog.Actividades;

import android.graphics.Typeface;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hachepe.irisblog.Models.Usuario;

import com.example.hachepe.irisblog.R;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Recuperar_Pass extends AppCompatActivity {
    private Button btnRecup;
    private EditText inputEmail;
    private TextView txtIntro, txtnota;

    private Registro r = new Registro();
    private final String CORREO_ADMIN = "IrisBlog.information@gmail.com";
    private final String CONTRA_ADMIN = "IRISBLOG007";
    private final String MENSAJE = "Su contraseña es la siguiente:";
    private final String SUBJECT = "IRIS BLOG - Recuperación de contraseña";
    private final String TRANSPORT = "smtp";
    private final String ERROR = "[ERROR] Estamos teniendo algún fallo técnico. Inténtelo más tarde.";
    private final String ENCODING = "text/html; charset=utf-8";
    private final String EMAIL_ERROR = "Email no registrado o incorrecto.";
    private final String EMAIL_INFO = "Contraseña enviada.";
    private final String HOST = "mail.smtp.host";
    private final String _HOST = "smtp.googlemail.com";
    private final String SFPORT = "mail.smtp.socketFactory.port";
    private final String _SFPORT = "465";
    private final String SFCLASS = "mail.smtp.socketFactory.class";
    private final String _SFCLASS = "javax.net.ssl.SSLSocketFactory";
    private final String AUTH = "mail.smtp.auth";
    private final String _AUTH = "true";
    private final String PORT = "mail.smtp.port";
    private final String _PORT = "465";
    Session s;
    private final String VACIO = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_pass);
        String font_path = "fonts/CaviarDreams.ttf";
        btnRecup = (Button) findViewById(R.id.btnRec);
        inputEmail = (EditText) findViewById(R.id.idemail);
        txtIntro = (TextView) findViewById(R.id.txtIntro);
        txtnota = (TextView) findViewById(R.id.txtnota);

        /**
         * Asignar fuente
         */
        Typeface caviar = Typeface.createFromAsset(getAssets(), font_path);
        txtIntro.setTypeface(caviar);
        inputEmail.setTypeface(caviar);
        btnRecup.setTypeface(caviar);
        txtnota.setTypeface(caviar);
    }

    /**
     * Método volver que regresa a la pantalla de Login
     *
     * @param view
     */
    public void volver_click(View view) {
        finish();
    }

    /**
     * Método asociado al boton que envia la contraseña
     *
     * @param view
     */
    public void enviarContrasenia(View view) {
        if (comprobarUsuario() == false) {
            Toast.makeText(getApplicationContext(), EMAIL_ERROR, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), EMAIL_INFO, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * Método que comprueba si el email introducido corresponde a un usuario registrado
     *
     * @return correcto
     */
    public boolean comprobarUsuario() {
        boolean correcto = true;
        if (inputEmail.getText().equals(VACIO) || r.getListaUsuarios().isEmpty()) {
            correcto = false;
        } else{
            for (Usuario u : r.getListaUsuarios()) {
                if (!inputEmail.getText().toString().equals(u.getEmail().toString())) {
                    correcto = false;
                } else {
                    sendMail(u);
                    break;
                }
            }
    }
        return correcto;
}

    /**
     * Método que envia el email al usuario
     *
     * @param usuario
     */
    public void sendMail(Usuario usuario) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Properties properties = new Properties();
        properties.put(HOST, _HOST);
        properties.put(SFPORT, _SFPORT);
        properties.put(SFCLASS, _SFCLASS);
        properties.put(AUTH, _AUTH);
        properties.put(PORT, _PORT);

        try {
            s = s.getDefaultInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(CORREO_ADMIN, CONTRA_ADMIN);
                }
            });

            if (s != null) {
                Message message = new MimeMessage(s);
                message.setFrom(new InternetAddress(CORREO_ADMIN));
                message.setSubject(SUBJECT);
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(usuario.getEmail().toString())); // Correo del Usuario a enviar la contraseña
                message.setContent(MENSAJE + " " + usuario.getContrasenia().toString(), ENCODING);

                Transport t = s.getTransport(TRANSPORT);
                t.connect(CORREO_ADMIN, "");
                t.sendMessage(message, message.getAllRecipients());
                t.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), ERROR, Toast.LENGTH_SHORT).show();
        }
    }


}
