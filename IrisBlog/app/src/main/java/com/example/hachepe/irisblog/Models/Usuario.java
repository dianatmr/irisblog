package com.example.hachepe.irisblog.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Roberto on 24/11/2017.
 */

public class Usuario implements Serializable{
    private String nombre;
    private String email;
    private String contrasenia;
    private ArrayList<Entrada> listaEntradas;
    private String idUser;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public ArrayList<Entrada> getListaEntradas(){
        return listaEntradas;
    }

    public String getIdUser(){
        return idUser;
    }

    public Usuario(String idUser, String nombre, String email, String contrasenia, ArrayList<Entrada> listaEntradas) {
        super();
        this.idUser = idUser;
        this.nombre = nombre;
        this.email = email;
        this.contrasenia = contrasenia;
        this.listaEntradas = listaEntradas;
    }

    public Usuario(String idUser, String nombre, String email, String contrasenia) {
        super();
        this.idUser = idUser;
        this.nombre = nombre;
        this.email = email;
        this.contrasenia = contrasenia;
    }

}
