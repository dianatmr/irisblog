/**
 * @author IrisGroup
 * @since 1.0
 */
package com.example.hachepe.irisblog.Actividades;


import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hachepe.irisblog.Models.Entrada;
import com.example.hachepe.irisblog.Models.Usuario;
import com.example.hachepe.irisblog.Otras_Clases.Comunicador;
import com.example.hachepe.irisblog.Otras_Clases.ComunicadorEntradas;
import com.example.hachepe.irisblog.R;

import java.util.ArrayList;

public class Login extends AppCompatActivity {
    private Button btnLogin, btnReg;
    private EditText inputUser;
    private EditText inputPassword;
    private TextView title, rec;
    private ImageView eye;
    private Animation slideLeft, slideRight;
    private static final int REQUEST_SIGNUP = 0;
    private static final String TAG = "LoginActivity";
    private final String EMAIL_ADMIN = "admin";
    private final String PASS_EMAIL = "admin";
    private Registro r;

    private final String VACIO = "";
    private static Usuario UsuarioConectado = null;



    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inputUser = (EditText) findViewById(R.id.edUser);
        inputPassword = (EditText) findViewById(R.id.edPass);
        btnLogin = (Button) findViewById(R.id.btnSign);
        title = (TextView) findViewById(R.id.txtTitle);
        rec = (TextView) findViewById(R.id.txtRecord);
        eye = (ImageView) findViewById(R.id.imageeye);
        btnReg = (Button) findViewById(R.id.btnReg);


            Usuario admin = new Usuario("admin","admin","admin@iris.com","admin");
            Usuario pepe = new Usuario("pepe","pepe","pepe@iris.com","pepe");
            Comunicador.setObjeto(admin);
            Comunicador.setObjeto(pepe);

         Entrada uno=new Entrada("Blog1", "admin", "La guerra asimétrica de Amazon", "El pasado mes de junio Tim Cook subía al escenario del centro de conventions McEnery de San José para inaugurar el evento de de desarrolladores WWDC. Como en ediciones anteriores, Apple aprovechó el evento para hablar de las novedades en sus diferentes plataformas y en el caso de TvOS -la versión de iOS que usa el AppleTV- la lista era corta pero incluía un anuncio muy esperado. Amazon Prime Video llegaría por fin este año al dispositivo.", true,
                "01/12/2017", "Cosas");
         Entrada dos=new Entrada("Blog2","admin", "El superordenador más potente del mundo ya es 100% chino", "China ya no necesita tecnología extranjera para subir a lo alto del podio de la supercomputación. El ordenador más veloz hasta la fecha, el Tianhe-2, acaba de ser sobrepasado por una nueva máquina en el centro de supercomputación nacional de la región de Wuxi, el Sunway TaihuLight. Lo interesante de este ordenador no es sólo su descomunal potencia de cálculo, que casi triplica a la de Tianhe-2, sino el hecho de que tanto sus procesadores como su arquitectura son 100% nacionales.\n" +
                "\n" +
                "La máquina anterior, aún activa en el centro de supercomputación de la región de Guangzhou y ahora el segundo ordenador más potente del mundo, utilizaba chips de la marca norteamericana Intel para alcanzar su alto rendimiento. Estados Unidos prohibió la venta de chips para supercomputadoras a China en abril de 2015 por motivos de seguridad pero la prohibición, al final, sólo ha servido para estimular el desarrollo de esta alternativa local.",false,
                "12/12/2017", "String subtitulo");
         Entrada tres=new Entrada("Blog3","admin", "Firefox ha vuelto con ganas de pelea", "String contenido3", true,
                "09/12/2017", "String subtitulo");
         Entrada cuatro=new Entrada("Blog4","pepe", "Así es Internet en 2017", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed nisi a neque egestas congue. Phasellus consectetur, mi non rutrum pretium, sapien libero posuere ante, vel gravida quam dolor et est. In hac habitasse platea dictumst. Morbi vel purus sapien. Vestibulum pulvinar lorem in lobortis imperdiet. Donec fringilla facilisis risus, non iaculis ante cursus eu. Maecenas et arcu sed arcu consectetur placerat nec in tortor. Pellentesque libero nulla, interdum ullamcorper aliquam a, semper id diam.", true,
                "01/12/2017", "Cosas");
         Entrada cinco=new Entrada("Blog5","pepe", "Intel y AMD, extraños compañeros de cama", "String contenidof",false,
                "12/12/2017", "String subtitulo");
         Entrada seis=new Entrada("Blog6","manolo", "Dos semanas mirando un iPhone X", "String contenido3", true,
                "09/12/2017", "String subtitulo");

        ComunicadorEntradas.setObjeto(uno);
        ComunicadorEntradas.setObjeto(dos);
        ComunicadorEntradas.setObjeto(tres);
        ComunicadorEntradas.setObjeto(cuatro);
        ComunicadorEntradas.setObjeto(cinco);
        ComunicadorEntradas.setObjeto(seis);

        /**
         * Iniciar sesión con Enter
         */
        inputPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnLogin.performClick();
                    return true;
                }
                return false;
            }
        });
        /**
         * Importar fuente en app/assets/fonts/...
         */
        final String font_path = "fonts/CaviarDreams.ttf";
        final Typeface caviar = Typeface.createFromAsset(getAssets(), font_path);

        /**
         * Asigna fuente a los atributos
         */
        title.setTypeface(caviar);
        rec.setTypeface(caviar);
        inputUser.setTypeface(caviar);
        inputPassword.setTypeface(caviar);
        btnLogin.setTypeface(caviar);
        btnReg.setTypeface(caviar);

        /**
         * Animaciones slide right-left/left-right
         * en app/res/anim/slide...
         */
        slideLeft = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideinleft);
        slideRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideinright);

        /**
         * Animaciones asignadas a los campos de texto y botón
         */
        inputUser.startAnimation(slideLeft);
        inputPassword.startAnimation(slideRight);
        btnLogin.startAnimation(slideLeft);

        /**
         * Animación del logo, latido de corazón
         * en app/res/anim/pulse.xml
         */
        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        eye.startAnimation(pulse);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });


        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Registro.class);
                startActivityForResult(i, REQUEST_SIGNUP);
            }
        });
    }

    public void login() {
        if (!validate()) {
            onLoginFailed();
            return;
        }
        btnLogin.setEnabled(false);

        String user = inputUser.getText().toString();
        String pass = inputPassword.getText().toString();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        btnLogin.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Inicio de sesión fallido", Toast.LENGTH_SHORT).show();
        btnLogin.setEnabled(true);
    }

    public void recuperar(View view) {
        Intent i = new Intent(this, Recuperar_Pass.class);
        startActivity(i);
    }

    /**
     * Metodo para saber si el login es valido
     * se crea un arraylist de usuarios para cargar los usuarios del comunicador para poder comprobar si existe uno con el nombre y contraseña correctos
     * @return
     */
    public boolean validate() {
        boolean valido = true;
        String user = inputUser.getText().toString();
        String pass = inputPassword.getText().toString();

        ArrayList<Usuario> objetousuario = new ArrayList<Usuario>();
        for (int i = 0; i< Comunicador.getObjeto().size(); i++){
            Usuario obj=(Usuario) Comunicador.getObjeto().get(i);
            objetousuario.add(obj);
        }
       if (comprobarUsuario(objetousuario) == true) {
            Intent i = new Intent(this, NavigationDrawer.class);
           i.putExtra("Usuario", inputUser.getText().toString());
            valido = true;
            startActivity(i);
           finish();
       }else {
           valido = false;
       }

        if (user.equals(EMAIL_ADMIN) && pass.equals(PASS_EMAIL)) {
            Intent i = new Intent(getApplicationContext(), NavigationDrawer.class);
            i.putExtra("Usuario", inputUser.getText().toString());

            valido = true;
            startActivity(i);
            finish();
            Toast.makeText(this, "Bienvenido ADMIN", Toast.LENGTH_SHORT).show();
        } else if (!(user.isEmpty() && pass.isEmpty())) {
            if (comprobarUsuario(objetousuario) == false) {
                valido = false;
            }
        } else {
            if (user.isEmpty()) {
                inputUser.setError("El campo de usuario esta vacío");
            } else {
                inputUser.setError(null);
            }
            if (pass.isEmpty() || pass.length() < 4 || pass.length() > 10) {
                inputPassword.setError("La contraseña es entre 4 y 10 caracteres");
            } else {
                inputPassword.setError(null);
            }
            valido = false;
        }
        return valido;

    }

    /**
     * Método que comprueba si el usuario está registrado
     *
     * @return
     */
    private boolean comprobarUsuario(ArrayList<Usuario> objetousuario) {
        boolean usuarioRegistrado = false;

        for (int i = 0; i< objetousuario.size(); i++){
            Usuario preguntarporusuarip=objetousuario.get(i);
            if(preguntarporusuarip.getNombre().toString().equals(inputUser.getText().toString())&& inputPassword.getText().toString().equals(preguntarporusuarip.getContrasenia())){
                usuarioRegistrado=true;
                break;
            }else{
                usuarioRegistrado=false;
            }
        }
        return usuarioRegistrado;
    }

    /**
     * Método que inicia sesión si el usuario está registrado
     */
    private void iniciarSesion(Usuario usuario) {
        Intent i = new Intent(this, NavigationDrawer.class);
        Bundle b = new Bundle();
        b.putSerializable("UsuarioConectado", usuario);
        i.putExtras(b);
        startActivity(i);
        finish();
        Toast.makeText(this, "Bienvenido " + getUsuarioConectado().getNombre(), Toast.LENGTH_SHORT).show();
    }

    /**
     * Método que crear un usuario auxiliar del usuario que está accediendo
     * @param u
     * @return
     */
    public Usuario creaUsuarioConectado(Usuario u) {
        Usuario usuario = new Usuario(u.getIdUser(), u.getNombre(), u.getEmail(), u.getContrasenia(), u.getListaEntradas());
        return usuario;
    }

    public Usuario getUsuarioConectado() {
        return UsuarioConectado;
    }


}

