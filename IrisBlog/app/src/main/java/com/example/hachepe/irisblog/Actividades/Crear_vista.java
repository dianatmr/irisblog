package com.example.hachepe.irisblog.Actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.example.hachepe.irisblog.R;

import java.util.ArrayList;

/**
 * Created by Roberto on 11/12/2017.
 * @deprecated Esta clase se utiliza para leer los datos de una Entrada reutilizando la activity de crear entrada
 *
 */


public class Crear_vista extends AppCompatActivity {
       private EditText Titulo, subtitulo, contenido;
        private Button publicar;
        private RadioButton Publico, Privado;
        private RadioGroup radioGroup;
        private ImageButton imageButton;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_crearentrada);
            Titulo = (EditText) findViewById(R.id.edTitulo);
            subtitulo = (EditText) findViewById(R.id.edSubTitulo);
            contenido = (EditText) findViewById(R.id.editText5);
            publicar = (Button) findViewById(R.id.btnPublicar);
            imageButton = (ImageButton) findViewById(R.id.imageButton);
            radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
            Publico = (RadioButton) findViewById(R.id.Publico);
            Privado = (RadioButton) findViewById(R.id.Privado);

            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            if (extras != null) {//ver si contiene datos

                Privado.setVisibility(View.INVISIBLE);
                Privado.setEnabled(false);
                Publico.setVisibility(View.INVISIBLE);
                Publico.setEnabled(false);
                radioGroup.setVisibility(View.INVISIBLE);
                radioGroup.setEnabled(false);

    /**
    * auxtitulo auxsubtitulo auxcontenido se utilizan para cargar los datos
     * pasados por un bundle en un intent
    */
                String auxtitulo = (String) extras.get("titulo");
                String auxsubtitulo = (String) extras.get("subtitulo");
                String auxcontenido = (String) extras.get("contenido");

                Titulo.setText(auxtitulo);
                subtitulo.setText(auxsubtitulo);
                contenido.setText(auxcontenido);

                Titulo.setEnabled(false);
                subtitulo.setEnabled(false);
                contenido.setEnabled(false);
                publicar.setEnabled(false);
                publicar.setVisibility(View.INVISIBLE);

            }
        }


    public void retroceder(View view){
        finish();
    }

    }

