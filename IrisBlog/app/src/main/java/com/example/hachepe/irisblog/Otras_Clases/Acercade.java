package com.example.hachepe.irisblog.Otras_Clases;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.hachepe.irisblog.R;

public class Acercade extends AppCompatActivity {
    private ImageView eye;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acercade);
        eye = (ImageView) findViewById(R.id.acercade);
        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        eye.startAnimation(pulse);
    }
}
