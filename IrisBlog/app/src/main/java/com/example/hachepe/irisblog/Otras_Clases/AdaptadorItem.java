package com.example.hachepe.irisblog.Otras_Clases;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hachepe.irisblog.Models.Entrada;
import com.example.hachepe.irisblog.R;

import java.util.ArrayList;

/**
 * Created by Roberto on 07/12/2017.
 */

public class AdaptadorItem extends BaseAdapter {

        protected Activity activity;
        protected ArrayList<Entrada> items;

        public AdaptadorItem (Activity activity, ArrayList<Entrada> items) {
            this.activity = activity;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        public void clear() {
            items.clear();
        }

        public void addAll(ArrayList<Entrada> category) {
            for (int i =0 ; i < category.size(); i++) {
                items.add(category.get(i));
            }
        }

        @Override
        public Object getItem(int arg0) {
            return items.get(arg0);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (convertView == null) {
                LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inf.inflate(R.layout.elemento, null);
            }

            Entrada dir = items.get(position);

            TextView title = (TextView) v.findViewById(R.id.txvTitulo);
            title.setText(dir.getTitulo());

            TextView description = (TextView) v.findViewById(R.id.tvCreador);
            description.setText(dir.getId_user());


            return v;
        }
    }

