package com.example.hachepe.irisblog.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hachepe.irisblog.Actividades.Crear_vista;
import com.example.hachepe.irisblog.Actividades.NavigationDrawer;
import com.example.hachepe.irisblog.Actividades.Registro;

import com.example.hachepe.irisblog.Models.Entrada;

import com.example.hachepe.irisblog.Models.Usuario;
import com.example.hachepe.irisblog.Otras_Clases.AdaptadorItem;

import com.example.hachepe.irisblog.Otras_Clases.Comunicador;
import com.example.hachepe.irisblog.Otras_Clases.ComunicadorEntradas;
import com.example.hachepe.irisblog.R;
import com.example.hachepe.irisblog.Actividades.Crear_entrada;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Michael on 24/11/2017.
 */

public class Biblioteca extends Fragment {
    public ListView listView;
    public TextView nombre;
    public ArrayList<Entrada> listademo=new ArrayList<Entrada>();
    public ArrayList<Entrada> ver=new ArrayList<Entrada>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_biblioteca, container, false);
        nombre=(TextView) getActivity().findViewById(R.id.textView3);
        String nombreCreador=nombre.getText().toString();
        for (int i = 0; i< ComunicadorEntradas.getObjeto().size(); i++){
            Entrada obj=(Entrada) ComunicadorEntradas.getObjeto().get(i);
            if(obj.getId_user().equals(nombreCreador) ) {
                listademo.add(obj);
            }


        }
        Toast.makeText(getContext(),"NOMBRE:"+nombreCreador,Toast.LENGTH_LONG).show();

        listView = (ListView) view.findViewById(R.id.Lista);
        Entrada aux;



        AdaptadorItem adapter = new AdaptadorItem(getActivity(), listademo);


        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final int pos = position;

                Intent i = new Intent(getActivity(), Crear_vista.class);
                Entrada aux = listademo.get(pos);

                String auxTitulo = aux.getTitulo();
                String auxSubtitulo = aux.getSubtitulo();
                String auxContenido = aux.getContenido();

                i.putExtra("titulo", auxTitulo);
                i.putExtra("subtitulo", auxSubtitulo);
                i.putExtra("contenido", auxContenido);

                //lo iniciamos pasandole la intencion, con todos sus parametros guardados
                startActivity(i);
            }
        });



        return view;
    }


}
