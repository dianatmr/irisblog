package com.example.hachepe.irisblog.Actividades;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hachepe.irisblog.Models.Entrada;
import com.example.hachepe.irisblog.Otras_Clases.Comunicador;
import com.example.hachepe.irisblog.R;
import com.example.hachepe.irisblog.Models.Usuario;
import com.sun.mail.imap.protocol.ID;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Registro extends AppCompatActivity {
    private EditText usuario, pass, pass2, email;
    private Button btnReg;
    private TextView txtCuenta;
    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static ArrayList<Usuario> listaUsuarios = new ArrayList<>();
    private static ArrayList<Entrada> listaEntradas;
    private final byte MIN_TAM = 4;
    private final byte MAX_TAM = 10;
    private static Usuario nuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        usuario = (EditText) findViewById(R.id.usuario);
        pass = (EditText) findViewById(R.id.pass);
        pass2 = (EditText) findViewById(R.id.pass2);
        email = (EditText) findViewById(R.id.email);
        txtCuenta = (TextView) findViewById(R.id.txtCrearCuenta);
        btnReg = (Button) findViewById(R.id.btnRegistro);
        final String font_path = "fonts/CaviarDreams.ttf";
        final Typeface caviar = Typeface.createFromAsset(getAssets(), font_path);

        usuario.setTypeface(caviar);
        pass.setTypeface(caviar);
        pass2.setTypeface(caviar);
        email.setTypeface(caviar);
        pass2.setTypeface(caviar);
        btnReg.setTypeface(caviar);
        txtCuenta.setTypeface(caviar);

        // Gradiente del texto
        int[] color = {Color.rgb(72, 174, 217), Color.WHITE};
        float[] position = {0, 1};
        Shader.TileMode tile_mode0 = Shader.TileMode.REPEAT; // or TileMode.REPEAT;
        LinearGradient lin_grad0 = new LinearGradient(0, 0, 0, 200, color, position, tile_mode0);
        Shader shader_gradient0 = lin_grad0;
        txtCuenta.getPaint().setShader(shader_gradient0);

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!usuario.getText().toString().isEmpty() && !pass.getText().toString().isEmpty() && !pass2.getText().toString().isEmpty()) {
                    if (validarEmail(email.getText().toString())) {
                        if (pass.getText().toString().equals(pass2.getText().toString())) {
                            if ((pass.getText().toString().length() >= MIN_TAM && pass.getText().toString().length() <= MAX_TAM) && (pass2.getText().toString().length() >= MIN_TAM && pass2.getText().toString().length() <= MAX_TAM)) {
                                listaEntradas = new ArrayList<>();
                                nuevo = new Usuario(idUser(), usuario.getText().toString(), email.getText().toString(), pass.getText().toString());
                                Comunicador.setObjeto(nuevo);
                                listaUsuarios.add(nuevo);

                                if (listaUsuarios.contains(nuevo)) {
                                    Toast.makeText(getBaseContext(), "[INFORMATION] Se ha registrado con éxito.", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(Registro.this, NavigationDrawer.class);
                                    i.putExtra("Usuario",usuario.getText().toString());
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(getBaseContext(), "[ERROR] No se ha podido registrar.", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getBaseContext(), "[ERROR] Las contraseñas deben ser entre 4 y 10 caracteres.", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getBaseContext(), "[ERROR] Las contraseñas no coinciden.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getBaseContext(), "[ERROR] Formato de email no válido.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "[ERROR] Rellena todos los campos.", Toast.LENGTH_SHORT).show();

                }
            }
        });

        /**
         * Iniciar sesión con Enter
         */
        pass2.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnReg.performClick();
                    return true;
                }
                return false;
            }
        });

    }

    /**
     * Método que valida el formato del email
     *
     * @param email
     * @return
     */
    public boolean validarEmail(String email) {
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Método que devuelve la lista de Usuarios
     *
     * @return listaUsuarios
     */
    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    /**
     * Método que retorna el usuario que se acaba de registrar
     *
     * @return
     */
    public Usuario getNuevo() {
        return nuevo;
    }

    /**
     * Método que genera de el IDUser mediante un contador
     *
     * @return IDUser
     */
    private String idUser() {
        int IDUser = 0;
        ++IDUser;
        return String.valueOf(IDUser);
    }

}
