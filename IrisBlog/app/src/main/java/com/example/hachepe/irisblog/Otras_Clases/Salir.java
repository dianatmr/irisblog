package com.example.hachepe.irisblog.Otras_Clases;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.hachepe.irisblog.Actividades.Login;
import com.example.hachepe.irisblog.R;

public class Salir extends AppCompatActivity {
    private ImageView exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salir);
        exit = (ImageView) findViewById(R.id.exit);
        AnimationDrawable animation = (AnimationDrawable) exit.getDrawable();
        animation.start();

        new CountDownTimer(3600, 1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Intent i = new Intent(getApplicationContext(), Login.class);
                startActivity(i);
            }
        }.start();

    }
}
