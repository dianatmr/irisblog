package com.example.hachepe.irisblog.Actividades;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.hachepe.irisblog.Fragments.Biblioteca;
import com.example.hachepe.irisblog.Models.Entrada;
import com.example.hachepe.irisblog.Models.Usuario;
import com.example.hachepe.irisblog.Otras_Clases.AdaptadorItem;
import com.example.hachepe.irisblog.Otras_Clases.ComunicadorEntradas;
import com.example.hachepe.irisblog.R;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static com.example.hachepe.irisblog.R.id.usuario;

public class Crear_entrada extends AppCompatActivity {
    private EditText Titulo, subtitulo, contenido;
    private Button publicar;
    private RadioButton Publico, Privado;
    private RadioGroup radioGroup;
    private ImageButton imageButton;
    private Entrada entrada;

    // VARIABLES CONSTANTES
    private final String VACIO = "";

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crearentrada);

        Titulo = (EditText) findViewById(R.id.edTitulo);
        subtitulo = (EditText) findViewById(R.id.edSubTitulo);
        contenido = (EditText) findViewById(R.id.editText5);
        publicar = (Button) findViewById(R.id.btnPublicar);
        imageButton = (ImageButton) findViewById(R.id.imageButton);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        Publico = (RadioButton) findViewById(R.id.Publico);
        Privado = (RadioButton) findViewById(R.id.Privado);

        Intent intent = getIntent();

        final Bundle extras = intent.getExtras();
        final String nombreusuario=extras.getString("Usuario");


        if (extras != null) {//ver si contiene datos

                publicar.setOnClickListener(new View.OnClickListener() { // evento clic del boton
                    @Override
                    public void onClick(View v) {
                        crearEntrada(nombreusuario);

                    }
                });

            } else {
            Toast.makeText(this,"Por favor reinicie la aplicacion,no hemos podido averiguar quien es usted", Toast.LENGTH_SHORT).show();
            this.finish();

        }

/**
 * Metodo para crear una entrada
 * a este metodo le pasamos un string el cual es el nombre del usuario
 */
    }public void crearEntrada(String usuario) {
        Entrada entrada=new Entrada();
        if (comprobarEntrada()) {
            /**
             * en este primer if comprobamos si la entrada se quiere realizar de forma privada por lo cual enviamos un false al constructor de la misma
             * rellenamos el constructor de entrada
             * realizamos un toast para informar al usuario de que la operacion se a ejecutado
             * creamos un nuevo intent que nos envia a la pantalla de navegacion
             * le pasamos por argumento un string para seguir sabiendo quien es nuestro usuario
             */
             if (Privado.isChecked()) {

                entrada= new Entrada("IDBLOG",usuario, Titulo.getText().toString(), contenido.getText().toString(),false, "12/12/12", subtitulo.getText().toString());ComunicadorEntradas.setObjeto(entrada);
                Toast.makeText(this, usuario + " su entrada ha sido añadida a privado", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), NavigationDrawer.class);
                i.putExtra("Usuario",usuario);
                startActivity(i);

            }
             /**
              * en este primer if comprobamos si la entrada se quiere realizar de forma publica por lo cual enviamos un false al constructor de la misma
              * rellenamos el constructor de entrada
              * realizamos un toast para informar al usuario de que la operacion se a ejecutado
              * creamos un nuevo intent que nos envia a la pantalla de navegacion
              * le pasamos por argumento un string para seguir sabiendo quien es nuestro usuario
              */else if (Publico.isChecked()) {
                entrada= new Entrada("IDBLOG",usuario, Titulo.getText().toString(), contenido.getText().toString(),true, "12/12/12", subtitulo.getText().toString());
                ComunicadorEntradas.setObjeto(entrada);

                Toast.makeText(this, "admin" + " su entrada ha sido añadida a público", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), NavigationDrawer.class);
                i.putExtra("Usuario",usuario);
                startActivity(i);

            }


        } else {
            Toast.makeText(this, "[ERROR] No se admiten campos vacíos.", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Metodo el cual utilizamos para comprobar si los campos requeridos para crear una entrada estan rellenos
     * @return
     */

    private boolean comprobarEntrada() {
        boolean entradaLlena = true;
        if (Titulo.getText().equals(VACIO) || subtitulo.getText().equals(VACIO) || contenido.getText().equals(VACIO)) {
            entradaLlena = false;
        }
        else {
            if (Publico.isChecked() == false && Privado.isChecked() == false) {
                entradaLlena = false;
            } else {
                entradaLlena = true;
            }
        }
        return entradaLlena;
    }

    /**
     * Método que retrocede a la ventana anterior
     * @param view
     */
    public void retroceder(View view){
        finish();
    }


}

