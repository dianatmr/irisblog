package com.example.hachepe.irisblog.Models;

/**
 * Created by Roberto on 25/11/2017.
 */

public class Entrada {
    String id_blog;
    String id_user;
    String titulo;
    String contenido;
    Boolean estado;
    String fechapublicacion;
    String subtitulo;
    public String getId_blog() {
        return id_blog;
    }
    public void setId_blog(String id_blog) {
        this.id_blog = id_blog;
    }
    public String getId_user() {
        return id_user;
    }
    public void setId_user(String id_user) {
        this.id_user = id_user;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getContenido() {
        return contenido;
    }
    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
    public Boolean getEstado() {
        return estado;
    }
    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    public String getFechapublicacion() {
        return fechapublicacion;
    }
    public void setFechapublicacion(String fechapublicacion) {
        this.fechapublicacion = fechapublicacion;
    }
    public String getSubtitulo() {
        return subtitulo;
    }
    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }
    public Entrada(){
        super();
    }
    public Entrada(String id_blog, String id_user, String titulo, String contenido, Boolean estado,
                   String fechapublicacion, String subtitulo) {
        super();
        this.id_blog = id_blog;
        this.id_user = id_user;
        this.titulo = titulo;
        this.contenido = contenido;
        this.estado = estado;
        this.fechapublicacion = fechapublicacion;
        this.subtitulo = subtitulo;
    }

    @Override
    public String toString() {
        return ";"+id_blog +";"+ id_user + ";" + titulo +";"+ contenido + ";" + estado +";"+ fechapublicacion + ";" + subtitulo;
    }
}
